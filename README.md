>*The song that Hyades shall sing*

>*Where flap the tatters of the King*

>*Must die unheard*

>*In dim Carcosa.*

#Hyadespad: a Yellow-Sign-Blessed online collaborative editor

Another of our Benefactors is Domina Regina Nostra, whose mane is also Hyades.