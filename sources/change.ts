//Ch.ts
enum OpID {
    OP_SKIP,
    OP_INS,
    OP_DEL,
    OP_ATT
}

const OpCodes = ['S', 'I', 'D', 'A'];

enum OpAttr {
    META,
    F_BOLD,
    F_ITAL,
    F_UNDER,
    F_WIPE,
    F_AUTHOR
}


class Changeset {

    constructor(
        public rev: string, //starting revision-ID
        public ops: Operation[], //opcodes
        public nrev: string//, // new revision-name
        //public ch: string //charbank
    ) { }
    
    /// Generate from string
    public static fromString(s: string) {
        let j = s.indexOf('$');
        let z = s.substr(0, j);
        let a = z.split(';');
        let q: Operation[];
        q = [];
        let x: number[] = [];
        let d = s.substr(j + 1);
        for (let i = 2; i < a.length; ++i) {
            let o = Operation.fromString(a[i], x.length ? x : null);
            if (o.op != OpID.OP_ATT) {
                if (o.op === OpID.OP_INS) {
                    o.d = d.substr(0, o.arg + o.lns);
                    d = d.substr(o.arg + o.lns);
                }
                q.push(o);
                x = [];
            }
            else
                x.push(o.arg);
            //q[i - 2] = Operation.fromString(a[i]);
        }
        return new Changeset(a[0], q, a[1]);
    }

    //Canonical string representation
    public toString(): string {
        let a = this.rev;
        let ch = '';
        a += ';' + this.nrev;
        for (let i = 0; i < this.ops.length; ++i) {
            a += ';' + this.ops[i].toString();
            ch += this.ops[i].d ? this.ops[i].d : '';
        }
        a += '$' + ch;
        return a;
    }

    //Clone object
    public clone(): Changeset {
        let ops: Operation[] = [];
        for (let o of this.ops)
            ops.push(o);
        return new Changeset(this.rev, ops, this.nrev);
    }

    // Find position [op, chshift, lshift] by total chpos/lpos
    private loc(chc: number, lc: number): [number | boolean, number, number] {
        let r: [number | boolean, number, number] = [-1, -1, -1];
        let i = 0;
        for (i = 0; i < this.ops.length; i++) {
            if (this.ops[i].op == OpID.OP_DEL) continue; // DEL doesn't count into
            if (chc < this.ops[i].arg)
            { r = [i, chc, lc]; return r; }
            chc -= this.ops[i].arg; lc -= this.ops[i].lns;
        }
        r = [false, chc, lc];
        return r;
    }

    // Divide op into two [only for changeset-concatenating!]
    // Returns true if divided, false if it's end
    private split(i: number, ch: number, l:number) : boolean {
        let R = this.ops[i];
        if (ch == 0)
            return false;
        if (R.arg < ch)
            return false;
        
        let L = R.clone();
        if (L.op == OpID.OP_INS) {
            L.d = L.d.substr(0,ch+l);
            R.d = R.d.substr(ch+l);
        }
        L.arg = ch; L.lns = l;
        R.arg -= ch; R.lns -= l;

        this.ops.splice(i,0,L);

        return true;
    }

    //apply 'delete' into a changeset
    private del(ch: number, ln: number, o: Operation) {
        let j = this.loc(ch, ln);
        let delch = o.arg, delln = o.lns;
        if (j[0] === false) {
            let z = this.ops[this.ops.length-1];
            if (z.concat(o, j[1]+z.arg,j[2]+z.lns))
                return;
            this.ops.push(new Operation(OpID.OP_SKIP, j[1], j[2]));
        } else {
            let idx = Number(j[0]), skipch = j[1], skipln = j[2];
            let delop : Operation = null;
            while (delch > 0) {
                if (idx >= this.ops.length)
                    break;
                let x = this.ops[idx];
                switch (x.op) {
                    case OpID.OP_INS:
                        delop = null;
                        let s = x.d;
                        x.d = s.substr(0, skipch);
                        s = s.substr(skipch);
                        if (s.length >= (delch+delln)) {
                            x.d += s.substr(delch+delln); 
                            x.arg -= delch;
                            x.lns -= delln;
                            return;
                        }
                        let z = (s.split('\\n').length)-1;
                        delch -= s.length - z;
                        delln -= z;
                        if (skipch) {
                            skipch = 0;
                            idx++;
                            break;
                        }
                        this.ops.splice(idx,1);
                        break;
                    case OpID.OP_SKIP:
                        if (skipch) {
                            this.split(idx, skipch, skipln); //if 'start', we'd be out of here; if 'end', we still skip this
                            idx++;
                            skipch = skipln = 0;
                            break;
                        }
                        if (x.arg > delch) {
                            if (!delop) {
                                delop =  new Operation(OpID.OP_DEL, delch,delln);
                                this.ops.splice(idx,0,delop); // insert before current
                                delop = null;
                            } else {
                                delop.arg += delch;
                                delop.lns += delln;
                            }
                            x.arg -= delch; 
                            x.lns -= delln;
                            delch = delln = 0;
                            break;
                        }
                        // we delete it fully
                        if (!delop) {
                            delop = x;
                            x.op = OpID.OP_DEL;
                        } else {
                            delop.arg += x.arg;
                            delop.lns += x.lns;
                            this.ops.splice(idx,1);
                        }
                        delch -= x.arg; delln -= x.lns;
                        break;
                    case OpID.OP_DEL:
                        if (delop) {
                            delop.arg += x.arg;
                            delop.lns += x.lns;
                            this.ops.splice(idx,1);
                        } else {
                            delop = x;
                            idx++; //DEL changes not chpos
                        }
                        break;
                    default:
                        throw SyntaxError("Not a correct op!");
                }
            }
        }
        if (delch)
            this.ops.push(new Operation(OpID.OP_DEL, delch, delln));
    }

    //apply 'insert' into a changeset
    private ins(ch: number, ln: number, o: Operation) {
        let j = this.loc(ch, ln);
        let delch = o.arg, delln = o.lns;
        if (j[0] === false) {
            let z = this.ops[this.ops.length-1];
            if (z.concat(o, j[1]+z.arg,j[2]+z.lns))
                return;
            this.ops.push(new Operation(OpID.OP_SKIP, j[1], j[2]));
            this.ops.push(o);
            return;
        }
        let idx = Number(j[0]), skipch = j[1], skipln = j[2];
        let delop : Operation = null;
        let x = this.ops[idx];
        if ((skipch == 0) && (idx > 0)) {
            let z = this.ops[idx-1];
            if (this.ops[idx-1].concat(o, 0+z.arg, skipln+z.lns))
                return;
        }
        switch (x.op) {
            case OpID.OP_INS:
                if (x.concat(o, skipch, skipln))
                    return;
            case OpID.OP_SKIP:
            case OpID.OP_DEL:
                if (skipch)
                    this.split(idx++, skipch, skipln);
                this.ops.splice(idx,0,o);
                return;            
        }
    }

    private att(ch: number, ln: number, o: Operation){
        //TODO: DO!
    }

    public add(a: Changeset): boolean {
        if (a.rev != this.nrev)
            return false;
        this.nrev = a.nrev;
        let chpos = 0, lpos = 0;
        for (let o of a.ops) {
            switch (o.op) {
                case OpID.OP_DEL:
                    this.del(chpos, lpos, o);
                    break;
                case OpID.OP_INS:
                    this.ins(chpos, lpos, o);
                case OpID.OP_SKIP:
                    if ((o.op == OpID.OP_SKIP) && o.attrs)
                        this.att(chpos, lpos, o);
                    chpos += o.arg; lpos += o.lns;
                    break;
                default:
                    throw SyntaxError("No such OP!");
            }
        }
        return true;
    }
}

class Operation {
    public toString(): string {
        let s = '';
        if (this.attrs)
            for (let a of this.attrs)
                s += 'A' + a + ';';
        return s + OpCodes[this.op] + this.arg + (this.lns ? ':' + this.lns : '');
    }

    constructor(
        public op: OpID,
        public arg: number,
        public lns: number,
        public attrs?: number[], //TODO: make it 'Set'
        public d?: string) {
        /*if (op == OpID.OP_ATT)
            throw SyntaxError("No longer operation!");*/
        if (op == OpID.OP_DEL && attrs)
            throw SyntaxError("Attributes on deletion are forbidden!");
        if (op != OpID.OP_INS && d)
            throw SyntaxError("Data are allowed only on insertion!");
    }

    // INS_spec
    public concat(o: Operation, ch: number, ln: number): boolean {
        if (o.op != this.op)
            return false;
        if (this.attrs && !(o.attrs))
            return false;
        if (o.attrs && !(this.attrs))
            return false;
        if (this.attrs && o.attrs)
            for (let i of o.attrs)
                if (this.attrs.indexOf(i) == -1)
                    return false;
        if (ch > this.arg)
            return false;
        if (o.op == OpID.OP_INS)
            this.d = this.d.substr(0,ch+ln) + o.d + this.d.substr(ch+ln);
        
        this.arg += o.arg;
        this.lns += o.lns;
        return true;
    }

    public clone(): Operation {
        return new Operation(this.op, this.arg, this.lns, this.attrs ? this.attrs.map(function(v) { return v; }) : null, this.d);
    }
    public static fromString(s: string, a?: number[], d?: string): Operation {
        let o: OpID;
        for (let i = 0; i < OpCodes.length; ++i)
            if (s[0] === OpCodes[i])
                o = i;
        if (o != null) {
            let j: number = s.indexOf(':');
            let i: number;
            //let j:number = Number(s.substr(s.indexOf(':')));
            if (j == -1) {
                i = Number(s.substr(1));
                j = 0;
            }
            else {
                i = Number(s.substr(1, j - 1));
                j = Number(s.substr(j + 1));
            }
            return new Operation(o, i, j, a, d);
        }
        throw new SyntaxError('Invalid operation code');
    }


}