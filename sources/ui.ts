class PadPosition {
    constructor(
        public charpos: number,
        public linepos: number,
        public origin: boolean,
        public terminal: boolean
    ) { }
    public minus(f: PadPosition): PadPosition {
        return new PadPosition(this.charpos - f.charpos, this.linepos - f.linepos, false, false);
    }
}

class PadSelection {
    range: Range;
    start: PadPosition;
    end: PadPosition;
    focusStart: boolean;
    focusAttrs: string[];

    constructor(k: PadCS, public sel: Selection) {
        if (sel.rangeCount != 1)
            return;
        this.range = sel.getRangeAt(0);

        this.focusStart = false;
        this.focusAttrs = [];
        for (let z of sel.focusNode.parentElement.classList)
            this.focusAttrs.push(z);
        if (this.range.startOffset == sel.focusOffset)
            if (this.range.startContainer == sel.focusNode)
                this.focusStart = true;

        this.start = k.pad.getPos(this.range.startOffset, this.range.startContainer);
        if (this.range.collapsed)
            this.end = this.start;
        else
            this.end = k.pad.getPos(this.range.endOffset, this.range.endContainer);

    }
}

class UserInput {
    private _y: number;
    private _k: PadCS;
    private _hkC: Map<number, number>;

    

    private setSelection(a: PadPosition, b: PadPosition) {
        let st = this._k.pad.getContOff(a);
        let en = this._k.pad.getContOff(b);
        let s = window.getSelection();
        s.removeAllRanges();
        let rg = document.createRange();
        rg.setStart(st[0], st[1]);
        rg.setEnd(en[0], en[1]);
        s.addRange(rg);
    }

    private genRevStr(): string {
        let rv = this._k.currev.split('.');
        let s = this._k.currev + ';' + rv[0];
        s += '.u' + this._y + '.r' + (rv.length > 2 ? Number(rv[2].substr(1)) + 1 : 1);
        return s;
    }

    //TODO: make Ctrl-Bksp/Ctrl-Del
    private genDelete(final?: boolean, bkspc?: boolean, word?: boolean): string {
        let c = new PadSelection(this._k, window.getSelection());
        let st = c.start, d = c.end.minus(st);
        if (c.range.collapsed) {
            d = new PadPosition(1, 0, false, false);
            if (bkspc === false) {
                if (st.charpos == this._k.pad.length)
                    return '';
                if (st.terminal)
                    d.linepos += 1;
            }
            if (bkspc === true) {
                if ((st.linepos == 0) && (st.charpos == 0))
                    return '';
                if (st.origin) {
                    st.linepos -= 1; d.linepos += 1;
                }
                st.charpos -= 1;
            }
        }
        let s = this.genRevStr();
        s += ';S' + st.charpos + (st.linepos ? ':' + st.linepos : '');
        if (c.range.collapsed && !final)
            return s;
        s += ';D' + d.charpos + (d.linepos ? ':' + d.linepos : '');
        if (final) {
            this._k.userApply(Changeset.fromString(s + '$'));
            this.setSelection(st, st);
            return s + '$';
        }
        return s;
    }

    private mapBack(t: string[]): number[] {
        let ret: number[] = [];
        for (let a of this._k.attribs) {
            let x = t.indexOf(a[1].css);
            if (x != -1) {
                ret.push(a[0]);
            }
        }
        return ret;
    }

    private genInsert(s: string, aff?: boolean) {
        //console.log('**INSERTED** : ' + s);
        let cs = this.genRevStr();
        let sel = window.getSelection();
        cs = this.genDelete(false);
        let ps = new PadSelection(this._k, sel);
        if (aff) {
            let az = ps.focusAttrs;
            cs += (az.length ? ';A' + this.mapBack(az).join(';A') : '');
        }
        let lfs = s.split('\\n').length - 1;
        cs += ';A' + this._y + ';I' + (s.length - lfs) + (lfs ? ':' + lfs : '') + '$' + s;
        this._k.userApply(Changeset.fromString(cs));
        let st = ps.start;
        st.charpos += s.length - lfs;
        st.linepos += lfs;
        this.setSelection(st, st);
    }

    private genApply(a: number) {
        //get selection; else keep it stack of attrs-to-be-applied
    }

    private handleInput(e: Event): boolean {
        //Here we process browser attempts in making attrs.
        //Then we undo them and reshape text in our image,
        //   generating changeset and applying it.
        return true;
    }

    public handlePaste(e: ClipboardEvent): boolean {
        e.preventDefault();
        //Here we get the clipboard-contents;
        //   if it is strictly our stylish things, we extract them;
        //   else we use it as momently-inserted-plaintext.
        this.genInsert(e.clipboardData.getData('text/plain').replace('\n', '\\n'));
        return false;
    }

    private onKeyDowned(e: KeyboardEvent): boolean {
        if (e.keyCode == 9) { // Tab
            e.preventDefault();
            this.genInsert('\t');
            return false;
        }
        if ((e.keyCode == 46) || (e.keyCode == 8)) { //Delete or Backspace
            e.preventDefault();
            this.genDelete(true, (e.keyCode == 8), e.ctrlKey);
        }
        if (e.keyCode == 13) {
            this.genInsert('\\n');
            e.preventDefault();
        }
        //TODO: make it not-only-ctrl!!
        //TODO: add Ctrl-Z / Ctrl-Y!
        if (e.ctrlKey && !(e.altKey || e.metaKey || e.shiftKey) && (this._hkC.has(e.keyCode))) {
            e.preventDefault();
            this.genApply(this._hkC.get(e.keyCode));
        }
        //if (e.ctrlKey && )
        //console.log(e);
        return true;
    }

    private onKeyPressed(e: KeyboardEvent): boolean {
        //console.log(e);
        /*
        // Skip: Bksp=8, Tab, Enter=13, Shift=16, Ctrl, Alt,
        // PBrk=19, Caps, Esc=27
        // Firefox assigns keyCode=0 to any printable character
        //    and an ASCII(e.key) for it's 'which'.
        if ((e.keyCode > 1) && (e.keyCode < 32)) return true;
        // Skip: PgUp=33, PgDn, End, Home, LArr, UArr, RArr,
        // DArr=40, Ins=45, Del
        if ((e.keyCode > 32) && (e.keyCode < 48)) return true;
        // Skip: LeftWindow=91, RightWindow, Select (Rclick)
        if ((e.keyCode > 90) && (e.keyCode < 94)) return true;
        if ((e.keyCode >110)&&(e.keyCode < 146)) return true;*/

        /*e.preventDefault();
        if (e.keyCode == 25)
            return false;
        if (e.keyCode == 26)
            return false;
        
        this.genInsert(e.key);
        return false;*/
        if (e.keyCode == 0) { //Firefox printable chars
            if (e.ctrlKey || e.altKey || e.metaKey)
                return true;
            e.preventDefault();
            this.genInsert(e.key);
            return false;
        }
        if (e.which == 0) { //Firefox control chars, 'cept for BKSP
            return true;
        }
        if (e.charCode === e.keyCode)
            if (e.keyCode > 31) { // Chrome printable chars
                e.preventDefault();
                this.genInsert(e.key);
                return false;
            }

    }

    constructor(tukhes: HTMLBodyElement, kopf: PadCS, yid: number) {
        tukhes.contentEditable = 'true';
        this._y = yid;
        this._k = kopf;
        this._hkC = new Map<number, number>(
            [[73, OpAttr.F_ITAL],
            [66, OpAttr.F_BOLD],
            [85, OpAttr.F_UNDER]]);
        //document.addEventListener
        tukhes.addEventListener('paste', (e) => this.handlePaste(e));
        tukhes.addEventListener('input', (e) => this.handleInput(e));
        //tukhes.addEventListener('cut', (e)=>this.handleCut(e));
        document.addEventListener('keydown', (e) => this.onKeyDowned(e));
        document.addEventListener('keypress', (e) => this.onKeyPressed(e));
    }
}

var ui: UserInput;
function onWndLoad(e: Event) {
    thisPad = new PadCS();
    ui = new UserInput(document.getElementsByTagName('body')[0], thisPad, 11);
}

window.addEventListener('load', onWndLoad);