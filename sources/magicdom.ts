//TODO: FIXME: починить убирание атрибутов, авторов etc

class MagicDOMLetters {
    span: HTMLSpanElement;
    private _attrs: AttribSet;

    public splitBy(ch: number): MagicDOMLetters {
        if (ch == 0) {
            return this;
        }
        if (ch == this.span.textContent.length)
            return null;
        let s = this.span.textContent;
        this.span.textContent = s.substr(0, ch);
        return new MagicDOMLetters(this._attrs.clone(), s.substr(ch));
    }

    public concat(l: MagicDOMLetters): boolean {
        if (! l._attrs.equal(this._attrs)) return false;
        this.span.textContent += l.span.textContent;
        l.span.remove();
        l._attrs = null;
        return true;
    }

    //TODO: optimize remove/adding if it keeps the same
    public addAttrs(t: AttribSet) {
        this.span.classList.remove(...this._attrs.classes);
        this._attrs.add(t);        
        this.span.classList.add(...this._attrs.classes);
    }

    public get attrs(): AttribSet { return this._attrs; }
    public set attrs(t: AttribSet) {
        this.span.classList.remove(...this._attrs.classes);
        this._attrs = t;
        this.span.classList.add(...this._attrs.classes);
    }

    public get length(): number {
        return this.span.textContent.length;
    }

    constructor(a: AttribSet, s: string, split? : boolean) {
        this.span = document.createElement('span');
        //this._attrs = a;
        this._attrs = a;
        this.span.classList.add(...a.classes);
        this.span.textContent = s;
    }
}

class MagicDOMLine {
    private linediv: HTMLDivElement;
    private _lineno: number;

    private _empty: boolean;
    private _ph: HTMLSpanElement;
    private elems: MagicDOMLetters[];

    public getContOff (chP : number) : [Node, number] {
        let ret : [Node, number] = [null, 0];
        let i = 0;
        if (this.elems.length == 0)
            {ret[1] = 0;
            ret[0] = this._ph;
            return ret;}
        while (chP > this.elems[i].length)
            chP -= this.elems[i++].length;
        ret[1] = chP;
        ret[0] = this.elems[i].span.childNodes.item(0);
        return ret;
    }

    public getPos(charPos : number, contOffs : number, t : Node) : PadPosition {
        let q = t.parentElement;
        if (t.nodeType == t.ELEMENT_NODE)
            q = <HTMLElement>t;
        while (q && q.tagName.toUpperCase() != 'SPAN')
            q = q.parentElement;
        if (!q) return null;
        let i: number;
        if (q != this._ph)
            for (i = 0; i < this.elems.length; ++i) {
                if (this.elems[i].span == q)
                    break;
                charPos += this.elems[i].length;
            }
        let term = false, orig = false;;
        if (i == this.elems.length)
            return null;
        if ((i+1 == this.elems.length) || (i==0))
            {orig = (contOffs == 0); term = (contOffs == this.elems[i].length);}
        if (q == this._ph)
            {term = true;  charPos -= contOffs; orig = true;}
        return new PadPosition(charPos, this._lineno, orig, term);
    }

    public attrAll(t : AttribSet) : void {
        for (let z of this.elems)
            z.addAttrs(t);
    }

    public appendElem(l: MagicDOMLetters) {
        //if (!l.connect()) -- to produce not tons of similar spans
        if (l.span.textContent.length == 0)
            return;
        if (this.elems.length)
            if (this.elems[this.elems.length - 1].concat(l))
                return;
        this.elems.push(l);
        //FIXME: make it nice and obj-oriented!
        this.linediv.appendChild(l.span);
        if (this._empty)
            this.linediv.removeChild(this._ph);
        this._empty = false;
    }


    public splitBy(ch: number): MagicDOMLetters[] {
        if (this.elems.length == 0) return [];
        let cnt = 0;
        let i: number;
        for (i = 0; i < this.elems.length; ++i) {
            if (cnt + this.elems[i].length > ch)
                break;
            cnt += this.elems[i].length;
        }
        let coll: MagicDOMLetters[] = ((i < this.elems.length) ? [this.elems[i].splitBy(ch - cnt)] : []);
        while (this.elems.length > (i + 1)) //[i]' elem == last
            coll.push(this.elems.splice(i+1,1)[0]);
        return coll;
    }

    public delete() {
        this.linediv.parentElement.removeChild(this.linediv);
    }

    public removeElem(e: MagicDOMLetters) {
        let i: number;
        if ((i = this.elems.indexOf(e)) != -1)
            this.elems.splice(i, 1);
        if (e.span.parentElement)
            e.span.parentElement.removeChild(e.span);
        if (this.elems.length == 0)
            this.linediv.appendChild(this._ph);
    }

    public get lineno(): number { return this._lineno; }
    public set lineno(a: number) { this._lineno = a; this.linediv.setAttribute('lineid', '' + (a + 1)); }

    public get length(): number {
        let l = 1; // \n
        for (let x of this.elems)
            l += x.length;
        return l;
    }
    public get empty(): boolean { return this._empty; }

    constructor(cont: HTMLElement, id: string, bef?: MagicDOMLine) {
        this.linediv = document.createElement('div');
        this.linediv.id = id;
        if (bef) {
            cont.insertBefore(this.linediv, bef.linediv);
        } else {
            cont.appendChild(this.linediv);
        }
        this._empty = true;
        this._ph = document.createElement('span');
        this._ph.className = 'placeholder'; this._ph.innerText = ' '; this.linediv.appendChild(this._ph);
        this.elems = [];
    }
}
