class Attrib {
    constructor(
        public id: number,
        public type: OpAttr,
        public val: string | boolean,
        public css: string
    ) {
    }
}

class AttribSet {
    private _attrs: Attrib[];
    constructor() {
        this._attrs = [];
    }
    public applyAttr(z: Attrib) {
        for (let a of this._attrs)
            if (a.type == z.type) {
                a.val = z.val;
                a.css = z.css;
                a.id = z.id;
                return;
            }
        this._attrs.push(z);
    }
    public add(a: AttribSet) {
        for (let q of a._attrs)
            this.applyAttr(q);
    }
    public clone(): AttribSet {
        let r = new AttribSet();
        for (let q of this._attrs)
            r._attrs.push(new Attrib(q.id, q.type, q.val, q.css));
        return r;
    }
    public get empty(): boolean {
        return this._attrs.length == 0;
    }
    public clear(): void {
        this._attrs = [];
    }
    public get classes(): string[] {
        let r: string[] = [];
        for (let q of this._attrs)
            if (q.css.length)
                r.push(q.css);
        return r;
    }

    public equal(a: AttribSet): boolean {
        if (a._attrs.length != this._attrs.length)
            return false;
        //TODO: make attr-based, not css-based comparator!
        let tt = this.classes, aa = a.classes;
        for (let q of tt) {
            if (aa.indexOf(q) == -1)
                return false;
        }
        for (let q of aa) {
            if (tt.indexOf(q) == -1)
                return false;
        }
        return true;
    }
}

class PadEditor {
    //TODO: make it container-agnostic?
    private cont: HTMLBodyElement;
    private padname: string;
    private lines: MagicDOMLine[];

    private lineno: number;
    private nextId: number;

    private charpos: number;

    public get length(): number {
        let q = -1;
        for (let l of this.lines)
            q += l.length;
        return q;
    }

    public getContOff(a: PadPosition): [Node, number] {
        let chp = a.charpos;
        for (let i = 0; i < a.linepos; ++i)
            chp -= this.lines[i].length;

        return this.lines[a.linepos].getContOff(chp);
    }

    public getPos(contOffs: number, t: Node): PadPosition {
        let q = t.parentElement;
        if (t.nodeType == t.ELEMENT_NODE)
            q = <HTMLElement>t;
        while (q && q.tagName.toUpperCase() != 'DIV')
            q = q.parentElement;
        if (!q) return null;
        let i = Number(q.getAttribute('lineid')) - 1;
        let charPos = contOffs;
        for (let j = 0; j < i; ++j)
            charPos += this.lines[j].length;
        return this.lines[i].getPos(charPos, contOffs, t);
    }

    // Добавить новую строку
    // Понятный комментарий:    ['1','2'], 0 -> ['', '1','2'];
    //                                   , 1 -> ['1', '' ,'2']
    private insertNewLine(): MagicDOMLine {
        let bef: MagicDOMLine = (this.lineno == this.lines.length) ? null : this.lines[this.lineno];
        let l = new MagicDOMLine(this.cont, 'pad' + this.nextId++, bef);
        l.lineno = this.lineno;
        this.lines.push(null);
        for (let i = this.lines.length - 1; i > this.lineno; --i) {
            this.lines[i] = this.lines[i - 1];
            this.lines[i].lineno = i;
        }
        this.lines[this.lineno] = l;
        return l;
    }

    public startChange() {
        this.lineno = this.charpos = 0;
    }

    public insertLines(attr: AttribSet, text: string[]) {
        let leftovers: MagicDOMLetters[] = [];

        if (!this.lines[this.lineno].empty) {
            leftovers = this.lines[this.lineno].splitBy(this.charpos);
            for (let f of leftovers)
                this.lines[this.lineno].removeElem(f);
        }
        this.lines[this.lineno].appendElem(new MagicDOMLetters(attr.clone(), text.shift()));

        for (let s of text) {
            this.lineno++;
            this.insertNewLine();
            if (s.length)
                this.lines[this.lineno].appendElem(new MagicDOMLetters(attr.clone(), s));
        }

        this.charpos = this.lines[this.lineno].length;

        for (let lo of leftovers) {
            this.lines[this.lineno].appendElem(lo);
        }
    }

    public delete(chc: number, lc: number) {
        let stLno = this.lineno;
        let cl = this.lines[this.lineno];
        if (!cl.empty) {
            let left = cl.splitBy(this.charpos);
            let cur: MagicDOMLetters;
            let shd: boolean = false;
            while (left.length > 0) {
                shd = false;
                cur = left.shift();
                if (chc < cur.length)
                    break;
                chc -= cur.length;
                cl.removeElem(cur);
                shd = true;
                cur = null;
            }
            if (left.length || cur) { // we have already finished in here
                let kept = cur.splitBy(chc);
                if (!shd) // ?????
                    cl.removeElem(cur);
                cl.appendElem(kept);
                for (let q of left)
                    cl.appendElem(q);
                return;
            }
        }
        if (chc == 0) return;
        chc -= 1; //\\n
        let j: number;
        for (j = stLno + 1; j < this.lines.length; ++j) {
            if (chc < this.lines[j].length)
                break;
            chc -= this.lines[j].length;
        }
        // this means we remove fully lines from stLno+1 to j (non-incl); and from line #j we pick out the leftovers.
        if (j < this.lines.length) {
            let left = this.lines[j].splitBy(chc);
            for (let el of left)
                this.lines[stLno].appendElem(el);
        }
        let forRemoval = this.lines.splice(stLno + 1, j - stLno);
        for (let ln of forRemoval)
            ln.delete();
        for (let i = stLno + 1; i < this.lines.length; ++i)
            this.lines[i].lineno = i;
    }

    public applyAttr(chc: number, lc: number, attr: AttribSet) {
        let l = this.lines[this.lineno];
        let l1 = l.splitBy(this.charpos);
        let el: MagicDOMLetters;
        while (l1.length > 0) {
            el = l1.shift();
            l.removeElem(el); //- ??
            if (chc < el.length)
                break;
            el.addAttrs(attr);
            l.appendElem(el);
            el = null;
        }
        if (el) { // only if we fitted in one line
            let l2 = el.splitBy(chc);
            el.addAttrs(attr);
            l.appendElem(el);
            l.appendElem(l2);
            for (let e of l1)
                l.appendElem(e);
            return; // -? 
        }
        let j = this.lineno;
        for (j = j + 1; j < this.lines.length; j++) {
            let len = this.lines[j].length;
            if (chc < len)
                break;
            chc -= len;
            this.lines[j].attrAll(attr);
        }
        if (chc == 0) return;
        let elmz = this.lines[j].splitBy(chc);
        this.lines[j].attrAll(attr);
        for (let z of elmz)
            this.lines[j].appendElem(z);
    }

    public moveOnTo(chn: number, ln: number) {
        this.lineno = ln;
        for (let i = 0; i < ln; ++i)
            chn -= this.lines[i].length;
        this.charpos = chn;
    }

    constructor(public padId: string) {
        this.cont = document.getElementsByTagName('body')[0];
        this.padname = 'pad';
        this.lineno = this.charpos = this.nextId = 0;
        this.lines = [];
        this.insertNewLine();
    }
}

//TODO: make more obvious name
class PadCS {

    pad: PadEditor;
    attribs: Map<number, Attrib>;
    // Let's denote revisions like:
    // [^.].r[^.]
    // first part is rev-id from srv
    // second is for local unapplied revs
    currev: string;
    nowSending: boolean;
    serverSide: Changeset;
    sent: Changeset;
    userApplied: Changeset;
    sendTimer : any;
    ajaxRq : XMLHttpRequest;

    private attrByIds(a?: number[]): AttribSet {
        let attr = new AttribSet();
        if (a)
            for (let i of a)
                attr.applyAttr(this.attribs.get(i));
        return attr;
    }

    public apply(cs: Changeset): boolean {
        console.log(cs.toString());
        if (cs.rev != this.currev) {
            console.log('Uncompatible changesets!');
            return false;
        }
        let lineno = 0, chno = 0;
        this.pad.startChange();
        for (let op of cs.ops) {
            switch (op.op) {
                case OpID.OP_DEL:
                    this.pad.moveOnTo(chno, lineno);
                    this.pad.delete(op.arg, op.lns);
                    break;
                case OpID.OP_INS:
                    let shift = op.arg + op.lns;
                    //TODO: test without \\, just\n
                    let insch = op.d.split('\\n');
                    //ch = ch.substr(shift);
                    this.pad.moveOnTo(chno, lineno);
                    this.pad.insertLines(this.attrByIds(op.attrs), insch);
                //op.attrs = null;
                case OpID.OP_SKIP:
                    if ((op.op == OpID.OP_SKIP) && op.attrs) {
                        this.pad.moveOnTo(chno, lineno);
                        this.pad.applyAttr(op.arg, op.lns, this.attrByIds(op.attrs));
                    }
                    lineno += op.lns;
                    chno += op.arg;
                    break;
                default:
                    throw SyntaxError('Unrecognized op!');
            }
        }
        this.currev = cs.nrev;
        return true;
    }

    public send() : boolean {
        if (this.userApplied) {

        this.sent = this.userApplied;
        this.userApplied = null;

        this.ajaxRq.send('r=' + encodeURIComponent(this.sent.toString()));

        return true;
    }
    return false;
    }

    public srvApply(cs: Changeset) {
        if (this.apply(cs)) {
            if (this.serverSide)
                {   this.serverSide.add(this.sent);
                    this.serverSide.add(cs);
                }
            else
                this.serverSide = cs;
            
        }
        this.ajaxRq.open('POST', '/1.php', true);
        this.ajaxRq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }

    public userApply(cs: Changeset): boolean {
        if (this.apply(cs)) {
            if (this.userApplied)
                this.userApplied.add(cs);
            else
                this.userApplied = cs;
            console.log(this.userApplied.toString());
            return true;
        }
        return false;
    }
    constructor() {
        this.pad = new PadEditor('pad');
        this.currev = 'null';
        // TODO: load this from serv
        this.attribs = new Map<number, Attrib>([[0, new Attrib(0, OpAttr.META, true, 'meta')],
        [1, new Attrib(1, OpAttr.F_BOLD, true, 'bold')],
        [2, new Attrib(2, OpAttr.F_ITAL, true, 'italic')],
        [3, new Attrib(3, OpAttr.F_UNDER, true, 'underscore')],
        [4, new Attrib(4, OpAttr.F_WIPE, true, 'wipe')],
        [5, new Attrib(5, OpAttr.META, false, '')],
        [6, new Attrib(6, OpAttr.F_BOLD, false, '')],
        [7, new Attrib(7, OpAttr.F_ITAL, false, '')],
        [8, new Attrib(8, OpAttr.F_UNDER, false, '')],
        [9, new Attrib(9, OpAttr.F_WIPE, false, '')],
        [10, new Attrib(10, OpAttr.F_AUTHOR, 'op-hui', 'op-hui')],
        [11, new Attrib(11, OpAttr.F_AUTHOR, 'ubbser', 'ubbser')]]);
        this.userApplied = null;
        this.serverSide = null;
        this.sendTimer = setInterval(function() {thisPad.send();}, 500);
        this.ajaxRq = new XMLHttpRequest();
        this.ajaxRq.onreadystatechange = function () {    if (this.readyState == 4)
            if (this.status == 200)
                thisPad.srvApply(Changeset.fromString(this.responseText));};
        this.ajaxRq.open('GET', '/1.php', true);
        this.ajaxRq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        this.ajaxRq.send(null);
    }
}

var thisPad: PadCS;
