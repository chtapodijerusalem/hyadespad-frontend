<?php
$diff = 'null;r0;I4$abcd';

const OP_SKIP = 0;
const OP_INS = 1;
const OP_DEL = 2;
const OP_ATT = 3;
const OpCodes = ['S', 'I', 'D', 'A'];

class Operation{
    public $op;
    public $arg;
    public $lns;
    public $attrs;
    public $d;
    public function __construct($o, $a, $l, $t, $v) {
        if ($o == OP_DEL && isset($t)) 
            die("Attributes on deletion are forbidden!");
        if ($o != OP_INS && isset($v))
            die("Data on non-insertion is forbidden!");
        $this->op = $o;
        $this->arg = $a;
        $this->lns = $l;
        $this->attrs = $t;
        $this->d = $v;
    }

    public function toString() {
        $s = '';
        if (isset($this->attrs))
            foreach($this->attrs as $f)
                $s .= 'A' . $f . ';';
        $s .= OpCodes[$this->op] . $this->arg . (($this->lns) ? ':' . $this->lns : '');
        return $s;
    }

    public static function fromString($s, $a=NULL, $d=NULL) {
        for ($i = 0; $i < count(OpCodes); $i++)
            if ($s[0] == OpCodes[$i])
                $o = $i;
        if (!isset($o))
            die ("Invalid opcode");
        $j = strpos($s, ':');
        if ($j == false) { /*strpos returns FALSE*/
            $i = intval(substr($s, 1));
            $j = 0;
        } else {
            $i = intval(substr($s, 1, $j-1));
            $j = intval(substr($s, $j+1));
        }
        return new Operation($o, $i, $j, $a, $d);
    }

    public function concat($o, $ch, $ln) {
        if ($o->op != $this->op)
            return false;
        if (isset($this->attrs) || isset($o->attrs))
            return false;
        foreach($o->attrs as $i)
            if (!in_array($i, $this->attrs))
                return false;
        if ($ch > $this->arg)
            return false;
        if ($o->op == OP_INS)
            $this->d = substr($this->d, 0, $ch+$ln) . $o->d . substr($this->d, $ch+$ln);
        
        $this->arg += $o->arg;
        $this->lns += $o->lns;

        return true;
    }
}

class Changeset {
    public $rev;
    public $ops;
    public $nrev;

    public function __construct($r0, $o, $r1){
        $this->rev = $r0; 
        $this->ops = $o;
        $this->nrev = $r1;
    }
    
    public function toString() {
        $s = '' . $this->rev . ';' . $this->nrev;
        $c = '';
        foreach($this->ops as $o) {
            $s .= ';' . $o->toString();
            $c .= ($o->d) ? $o->d : '';
        }
        return $s . '$' . $c;
    }

    public static function fromString($s){
        $j = strpos($s, '$');
        $z = substr($s, 0, $j);
        $a = explode(';', $z);
        $q = [];
        $x = [];
        $d = substr($s, $j+1);
        for ($i = 2; $i < count($a); $i++) {
            $o = Operation::fromString($a[$i], (count($x) ? $x : null));
            if ($o->op != OP_ATT) {
                if ($o->op == OP_INS){
                    $o->d = substr($d, 0, $o->arg + $o->lns);
                    $d = substr($d, $o->arg + $o->lns);
                }
                $q[] = $o;
                $x = [];
            } else 
                $x[] = $o->arg;
        }
        return new Changeset($a[0], $q, $a[1]);
    }

}
//print('null;r0;I4$abcd');


//phpinfo();
print (var_dump(Changeset::fromString($diff)));
print(Changeset::fromString($diff)->toString());